package rabbitClient

import "github.com/streadway/amqp"

const (
	// ExchangeIsDurable defaults to true to ensure broker survives restart. Otherwise, a re-declaraction is required.
	ExchangeIsDurable = true

	// ExchangeType defaults to "topic", because topic is versatile to be either fanout or direct based on the binding and routing keypair
	ExchangeType = "topic"

	// QueueIsDurable defaults to true to ensure persistent messages within the queue are not lost if queue is restarted
	QueueIsDurable = true

	// PublishIsMandatory defaults to true to ensure message is actually delivered.
	// Message could be dropped if mandatory=false and there is no queue with the binding with the routingKey
	PublishIsMandatory = true

	// DeliveryMode defaults to 2, which means message is persistent and always get persisted to harddisk
	DeliveryMode = amqp.Persistent
)
