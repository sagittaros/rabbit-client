package main

import (
	"flag"
	"log"

	"gitlab.com/sagittaros/rabbit-client"
)

func main() {
	var queueName string
	flag.StringVar(&queueName, "queue", "test-queue", "Queue name")
	flag.Parse()

	subscriber := rabbitClient.DialAsSubscriber("amqp://guest:guest@localhost:5672", "test-exchange")
	defer subscriber.Close()

	subscription := subscriber.EnsureSubscription(
		queueName, // queue name
		"key",     // binding key
		false,     // exclusive
		1,         // prefetch count
	)

	// the listening function is executed on the main goroutine
	subscription.BlockingListen(func(body []byte) {
		log.Printf("Received request from [%s]: %s", queueName, body)
	})
}
