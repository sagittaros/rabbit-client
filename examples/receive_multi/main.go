package main

import (
	"log"

	"gitlab.com/sagittaros/rabbit-client"
)

func main() {
	subscriber := rabbitClient.DialAsSubscriber("amqp://guest:guest@localhost:5672", "test-exchange")
	defer subscriber.Close()

	subscription1 := subscriber.EnsureSubscription(
		"test-queue-1", // queue name
		"key-1",        // binding key
		false,          // exclusive
		1,              // prefetch count
	)
	subscription2 := subscriber.EnsureSubscription(
		"test-queue-2", // queue name
		"key-2",        // binding key
		false,          // exclusive
		1,              // prefetch count
	)

	subscription1.Listen(func(body []byte) {
		log.Printf("Received request: %s", body)
	})

	subscription2.Listen(func(body []byte) {
		log.Printf("Received request: %s", body)
	})

	subscription1.BlockExecution()
}
