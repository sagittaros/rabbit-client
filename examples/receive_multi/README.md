How to trigger:

```
rabbitmqadmin publish exchange=test-exchange routing_key=key-1 payload="from key 1"
rabbitmqadmin publish exchange=test-exchange routing_key=key-1 payload="from key 1"
rabbitmqadmin publish exchange=test-exchange routing_key=key-2 payload="from key 2"
rabbitmqadmin publish exchange=test-exchange routing_key=key-2 payload="from key 2"
rabbitmqadmin publish exchange=test-exchange routing_key=key-1 payload="from key 1"
rabbitmqadmin publish exchange=test-exchange routing_key=key-2 payload="from key 2"
rabbitmqadmin publish exchange=test-exchange routing_key=key-1 payload="from key 1"
rabbitmqadmin publish exchange=test-exchange routing_key=key-1 payload="from key 1"
```