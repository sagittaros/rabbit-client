package main

import (
	"log"

	"gitlab.com/sagittaros/rabbit-client"
)

func main() {
	subscriber := rabbitClient.DialAsSubscriber("amqp://guest:guest@localhost:5672", "test-exchange")
	defer subscriber.Close()

	subscription := subscriber.EnsureSubscription(
		"",    // let broker decides
		"key", // binding key
		true,  // exclusive
		1,     // prefetch count
	)

	// the listening function is executed on the main goroutine
	subscription.BlockingListen(func(body []byte) {
		log.Printf("Received request from [exclusive queue]: %s", body)
	})
}
