package main

import (
	"log"

	"gitlab.com/sagittaros/rabbit-client"
)

func main() {
	publisher := rabbitClient.DialAsPublisher("amqp://guest:guest@localhost:5672", "test-exchange")
	defer publisher.Close()

	msg := `
			{
				"s3_document_uri": "test",
				"upload_id": "1234"
			}
		`
	publisher.PublishMessage("key", msg)
	log.Printf(" [x] Sent %s", msg)
}
