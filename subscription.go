package rabbitClient

import (
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/streadway/amqp"
)

type Subscription struct {
	Subscriber *Subscriber
	Channel    *amqp.Channel
	Queue      *amqp.Queue
	BindingKey string
	Exclusive  bool
}

// BlockingListen blocks the goroutine from continuing further
func (subscription *Subscription) BlockingListen(listener func([]byte)) {
	subscription.listen(listener)
	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	subscription.BlockExecution()
}

// Listen without blocking the calling goroutine
func (subscription *Subscription) Listen(listener func([]byte)) {
	subscription.listen(listener)
}

func (subscription *Subscription) listen(listener func([]byte)) {
	go func() {
		log.Printf("Connection Closed: %s", <-subscription.Channel.NotifyClose(make(chan *amqp.Error)))
		subscription.reconnect(listener)
	}()

	msgs, err := subscription.Channel.Consume(
		subscription.Queue.Name, // queue
		"",    // consumer
		false, // auto-ack
		subscription.Exclusive, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)
	handleClientErr(err, "Failed to register consumer", subscription)

	go func() {
		for d := range msgs {
			listener(d.Body)
			err := d.Ack(false) // `multiple=false`, only ack the current instance
			handleClientErr(err, "Unable to send ACK", subscription)
		}
	}()
}

// BlockExecution blocks the calling goroutine until os.Interrupt is fired
func (subscription *Subscription) BlockExecution() {
	signalChan := make(chan os.Signal, 1)
	forever := make(chan bool)
	signal.Notify(signalChan, os.Interrupt)
	go func() {
		<-signalChan
		close(forever)
	}()
	<-forever
}

func (subscription *Subscription) reconnect(listener func([]byte)) {
	log.Printf("Will attempt Reconnect in 30 seconds...")
	time.Sleep(30 * time.Second)
	log.Printf("Attempting Reconnect...")
	subscription.Subscriber.reconnect()
	newSubscription := subscription.Subscriber.EnsureSubscription(subscription.Queue.Name, subscription.BindingKey, subscription.Exclusive, 1)

	subscription.Channel = newSubscription.Channel
	subscription.Queue = newSubscription.Queue

	subscription.listen(listener)
}
