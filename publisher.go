package rabbitClient

import (
	"encoding/json"
	"log"
	"time"

	"github.com/streadway/amqp"
)

type Publisher struct {
	Connection      *amqp.Connection
	OutgoingChannel *amqp.Channel
	ConnectionInfo  map[string]string
}

// DialAsPublisher establishes a connection to broker and declare the exchange to use for the returned `Publisher`
// AMQP is too flexible. we reduce that flexibility and predictability by hard-fixing exchange name throughout an application lifecycle
// We should not see a single script handling multiple exchanges. If it does, it should be a separate TCP connection
func DialAsPublisher(connectionString, exchangeName string) *Publisher {
	publisher := &Publisher{}
	publisher.ConnectionInfo = map[string]string{
		"ConnectionString": connectionString,
		"ExchangeName":     exchangeName,
	}
	amqpConn, err := amqp.Dial(connectionString)
	handleClientErr(err, "Failed to connect to AMQP Broker", connectionString)
	publisher.Connection = amqpConn

	channel, err := amqpConn.Channel()
	handleClientErr(err, "Failed to open AMQP channel", publisher.ConnectionInfo)
	publisher.OutgoingChannel = channel

	err = channel.ExchangeDeclare(
		exchangeName,      // name
		ExchangeType,      // type
		ExchangeIsDurable, // durable
		false,             // auto-deleted
		false,             // internal
		false,             // no-wait
		nil,               // arguments
	)
	handleClientErr(err, "Failed to declare exchange", publisher.ConnectionInfo)
	publisher.watchConnection()
	return publisher
}

func (publisher *Publisher) PublishMessage(routingKey, message string) {
	returns := publisher.OutgoingChannel.NotifyReturn(make(chan amqp.Return, 1))

	err := publisher.OutgoingChannel.Publish(
		publisher.ConnectionInfo["ExchangeName"], // exchange
		routingKey,         // routing key
		PublishIsMandatory, // mandatory
		false,              // immediate
		amqp.Publishing{
			DeliveryMode: DeliveryMode,
			ContentType:  "text/plain",
			Body:         []byte(message),
		})
	handleClientErr(err, "Failed to publish message", map[string]string{
		"routing_key": routingKey,
		"message":     message,
	})

	go func() {
		for r := range returns {
			encoded, _ := json.Marshal(r) // nolint: fas
			log.Println(string(encoded))
		}
	}()
}

func (publisher *Publisher) Close() {
	err := publisher.Connection.Close()
	handleClientErr(err, "Failed to close AMQP connection", publisher.ConnectionInfo)
	log.Println("Publisher terminated gracefully")
}

func (publisher *Publisher) watchConnection() {
	go func() {
		log.Printf("Connection Closed: %s", <-publisher.OutgoingChannel.NotifyClose(make(chan *amqp.Error)))
		publisher.reconnect()
	}()
}

func (publisher *Publisher) reconnect() {
	log.Printf("Will attempt Reconnect in 30 seconds...")
	time.Sleep(30 * time.Second)
	log.Printf("Attempting Reconnect...")

	connectionInfo := publisher.ConnectionInfo
	newPublisher := DialAsPublisher(connectionInfo["ConnectionString"], connectionInfo["ExchangeName"])
	publisher.Connection = newPublisher.Connection
	publisher.OutgoingChannel = newPublisher.OutgoingChannel
}
