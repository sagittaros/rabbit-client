package rabbitClient

import (
	"log"

	"github.com/streadway/amqp"
)

type Subscriber struct {
	Connection     *amqp.Connection
	ConnectionInfo map[string]string
}

// DialAsSubscriber establishes a connection to broker and declare the exchange to use for the returned `Subscriber`
// AMQP is too flexible. we reduce that flexibility and predictability by hard-fixing exchange name throughout an application lifecycle
// We should not see a single script handling multiple exchanges. If it does, it should be a separate TCP connection
func DialAsSubscriber(connectionString, exchangeName string) *Subscriber {
	subscriber := &Subscriber{}
	subscriber.ConnectionInfo = map[string]string{
		"ConnectionString": connectionString,
		"ExchangeName":     exchangeName,
	}
	amqpConn, err := amqp.Dial(connectionString)
	handleClientErr(err, "Failed to connect to AMQP Broker", connectionString)
	subscriber.Connection = amqpConn

	mainChannel, err := amqpConn.Channel()
	handleClientErr(err, "Failed to open AMQP channel", subscriber.ConnectionInfo)

	err = mainChannel.ExchangeDeclare(
		exchangeName,      // name
		ExchangeType,      // type
		ExchangeIsDurable, // durable
		false,             // auto-deleted
		false,             // internal
		false,             // no-wait
		nil,               // arguments
	)
	handleClientErr(err, "Failed to declare exchange", subscriber.ConnectionInfo)
	err = mainChannel.Close()
	handleClientErr(err, "Failed to close AMQP channel", subscriber.ConnectionInfo)

	return subscriber
}

func (subscriber *Subscriber) reconnect() {
	// Close old connection
	subscriber.Close()
	connectionInfo := subscriber.ConnectionInfo
	newSubscriber := DialAsSubscriber(connectionInfo["ConnectionString"], connectionInfo["ExchangeName"])
	subscriber.Connection = newSubscriber.Connection
}

func (subscriber *Subscriber) EnsureSubscription(name, bindingKey string, exclusive bool, prefetch int) *Subscription {
	log.Printf("Initiating Connection")
	channel, err := subscriber.Connection.Channel()
	handleClientErr(err, "Failed to open AMQP channel", subscriber.ConnectionInfo)

	queue, err := channel.QueueDeclare(
		name,           // name
		QueueIsDurable, // durable
		false,          // delete when unused
		exclusive,      // exclusive
		false,          // no-wait
		nil,            // arguments
	)
	handleClientErr(err, "Failed to declare queue", name)

	err = channel.QueueBind(
		name,       // queue name
		bindingKey, // binding key
		subscriber.ConnectionInfo["ExchangeName"], // exchange
		false,
		nil)
	handleClientErr(err, "Failed to bind AMQP queue", name)

	err = channel.Qos(
		prefetch, // prefetch count
		0,        // prefetch size
		false,    // global
	)
	handleClientErr(err, "Failed to set QoS", prefetch)

	return &Subscription{
		Subscriber: subscriber,
		Channel:    channel,
		Queue:      &queue,
		BindingKey: bindingKey,
		Exclusive:  exclusive,
	}
}

func (subscriber *Subscriber) Close() {
	err := subscriber.Connection.Close()

	// If the connection is not open in the first place, no need to get all bitchy about it.
	if err != nil && err.Error() != "Exception (504) Reason: \"channel/connection is not open\"" {
		handleClientErr(err, "Failed to close AMQP connection", subscriber.ConnectionInfo)
	}
	log.Println("Subscriber terminated gracefully")
}
