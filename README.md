# Rabbit Client

A "topic" only rabbit-client with [thoughtful defaults](app_defaults.go) and hopefully good abstraction (We try to do better)

## Features

* Publisher that uses `routing_key` and `payload`
* Single subscriber can have multiple subscriptions
* Blocking and non-blocking subscriptions

## How to use this

Start by looking at [examples](examples)