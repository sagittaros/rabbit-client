# v0.1.0

first release

* Publisher that uses `routing_key` and `payload`
* Single subscriber can have multiple subscriptions
* Blocking and non-blocking subscriptions