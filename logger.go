package rabbitClient

import (
	"encoding/json"
	"log"
)

func handleClientErr(err error, msg string, extras interface{}) {
	if err == nil {
		return
	}
	extrasASJson, _ := json.Marshal(extras)
	log.Fatalf("%s: %s\nExtraInfo: %s",
		msg,
		err,
		string(extrasASJson),
	)
}
